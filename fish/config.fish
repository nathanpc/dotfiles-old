#set -Ux EDITOR vim
#set_export PATH /opt/arduino\x1e/opt/arduino\x1e/opt/microchip/xc8/v1\x2e12/bin\x1e/usr/local/sbin\x1e/usr/local/bin\x1e/usr/sbin\x1e/usr/bin\x1e/sbin\x1e/bin\x1e/usr/games\x1e/usr/local/games\x1e/usr/lib/jvm/java\x2d7\x2doracle/bin\x1e/usr/lib/jvm/java\x2d7\x2doracle/db/bin\x1e/usr/lib/jvm/java\x2d7\x2doracle/jre/bin
set __fish_init_1_22_0 \x1d
set __fish_init_1_50_0 \x1d
set __prompt_initialized_2 \x1d
set fish_color_autosuggestion 9e9e9e
set fish_color_command 005fd7\x1epurple
set fish_color_comment red
set fish_color_cwd green
set fish_color_cwd_root red
set fish_color_error red\x1e\x2d\x2dbold
set fish_color_escape cyan
set fish_color_history_current cyan
set fish_color_host \x2do\x1ecyan
set fish_color_match cyan
set fish_color_normal normal
set fish_color_operator cyan
set fish_color_param 00afff\x1ecyan
set fish_color_quote brown
set fish_color_redirection normal
set fish_color_search_match \x2d\x2dbackground\x3dpurple
set fish_color_status red
set fish_color_user \x2do\x1egreen
set fish_color_valid_path \x2d\x2dunderline
set fish_greeting 
set fish_key_bindings fish\x5fdefault\x5fkey\x5fbindings
set fish_pager_color_completion normal
set fish_pager_color_description 555\x1eyellow
set fish_pager_color_prefix cyan
set fish_pager_color_progress cyan

# Path settings.
#set fish_user_paths /opt/arduino\x1e/opt/hpgcc/2.0SP2/bin\x1e/opt/hpgcc/2.0SP2/cc/bin

# Other environment variables.
set -gx TERM rxvt-256color
set -gx HPGCC /opt/hpgcc/2.0SP2
set -gx HPAPINEROOT /opt/hpgcc/2.0SP2-hpapine

# Fish startup script.

screenfetch
